<%-- 
    Document   : datosvec
    Created on : 06-06-2021, 14:58:42
    Author     : acamposm
--%>
<%@page import="cl.util.Usuario"%>
<%@page import="com.entity.Informacionvecinos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    HttpSession session2 = request.getSession();

    Usuario usuario = (Usuario) session2.getAttribute("usuario");

    if (usuario == null) {

        request.getRequestDispatcher("index.jsp").forward(request, response);

    }
    Informacionvecinos vecino = (Informacionvecinos) request.getAttribute("vecino");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            *{text-decoration:none; list-style:none; margin:0px; padding:0px; outline:none;}
            body{margin:10px; padding:15px; font-family: 'Open Sans', sans-serif;}
            section{width:100%; max-width:1200px; margin:0px auto; display:table; position:relative;}
            h2{margin:10px auto;  font-size:24px; padding:20px 0px; color:#002e5b; text-align:left;}
            h2 span{font-weight:500;}

            header{width:100%; display:table; background-color:#FFFFFF; margin-bottom:50px;}
            #logo{float:left; font-size:24px; text-transform:uppercase; color:#002e5b; font-weight:600; padding:20px 0px;}
        </style>
    </head>
    <body>
        <header>

            <a href="yur" id="logo" target="_blank"></a>

            <label for="toggle-1" class="toggle-menu"><ul><li></li> <li></li> <li></li></ul></label>


            <section id="about" class="content">

                <h2>Información Vecino</h2>
                <p>


                <form  name="form" action="InformacionController" method="POST">
                    <% if ((usuario != null) && (vecino != null)) {%>

                    <table >
                        <tr >  
                        <div class="form-group">
                            <td width="100"><b>Rut</b></td>
                            <td><input  name="rut" value="<%= vecino.getRut()%>"  class="form-control" required id="rut" aria-describedby="usernameHelp"></td>
                        </div>
                        </tr> 
                        <tr>
                            <td height="15">&nbsp;</td>
                        </tr>

                        <tr >    
                        <div class="form-group">
                            <td width="100"><b>Nombres</b></td>
                            <td><input  step="any" name="nombres" value="<%= vecino.getNombres()%>"  class="form-control" required id="nombre" aria-describedby="nombreHelp"></td>
                        </div>       
                        </tr>
                        <tr>
                            <td height="15">&nbsp;</td>
                        </tr>
                        <tr >
                        <div class="form-group">
                            <td width="100"><b>Apellidos</b></td>
                            <td><input  step="any" name="apellidos" value="<%= vecino.getApellidos()%> "  class="form-control" required id="apellidos" aria-describedby="nombreHelp"></td>
                        </div> 
                        </tr> 
                        <tr>
                            <td height="15">&nbsp;</td>
                        </tr>
                        <tr >
                        <div class="form-group">
                            <td width="100"><b>Telefono</b></td>
                            <td><input  step="any" name="telefono" value="<%= vecino.getTelefono()%> "  class="form-control" required id="apellidos" aria-describedby="nombreHelp"></td>
                        </div> 
                        </tr> 
                        <tr>
                            <td height="15">&nbsp;</td>
                        </tr>
                        <tr >
                        <div class="form-group">
                            <td width="100"><b>Correo</b></td>
                            <td width="150"><input  step="any" name="correo" value="<%= vecino.getCorreo()%> "  class="form-control" required id="apellidos" aria-describedby="nombreHelp"></td>
                        </div> 
                        </tr>
                        <tr >
                            <td>
                                <br><br>
                            </td>
                        </tr>
                    </table><!-- comment -->
                    <table>
                        <tr > 
                            <td><button type="submit" name="accion" value="modificar" class="btn btn-success" onclick="alert('Vecino Actualizado!');">Actualizar Datos</button></td>
                            <td width="30"></td>
                            <td><button type="submit" name="accion" value="eliminar" class="btn btn-success" onclick="alert('Vecino Eliminado!');">Eliminar Datos</button></td>
                        </tr> 
                    </table>

                    <%}%>

                </form>

                </p>
            </section>
        </header>


    </body>
</html>
