<%-- 
    Document   : loginvec
    Created on : 06-06-2021, 11:54:02
    Author     : acamposm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            *{text-decoration:none; list-style:none; margin:0px; padding:0px; outline:none;}
            body{margin:10px; padding:15px; font-family: 'Open Sans', sans-serif;}
            section{width:100%; max-width:1200px; margin:0px auto; display:table; position:relative;}
            h2{margin:10px auto;  font-size:24px; padding:20px 0px; color:#002e5b; text-align:left;}
            h2 span{font-weight:500;}

            header{width:100%; display:table; background-color:#FFFFFF; margin-bottom:50px;}
            #logo{float:left; font-size:24px; text-transform:uppercase; color:#002e5b; font-weight:600; padding:20px 0px;}
        </style>

    </head>
    <body>
        <h2>Información de Vecinos</h2>
        <form name="form" action="RegistroController" method="POST">
            <table >
                <tr>
                    <td height="15">&nbsp;</td>
                </tr>
                <tr >
                    <td width="200"><b>Ingrese Rut</b></td><!--Celda de la etiqueta nombre-->
                    <td><input type="text" name="rut" id="rut"></td>
                </tr>
                <tr>
                    <td height="15">&nbsp;</td>
                </tr>
                <tr>
                    <td width="200"><b>Ingrese Nombres</b></td><!--Celda de la etiqueta Sección-->
                    <td><input type="text" name="nombres" id="nombres"></td>
                </tr>
                <tr>
                    <td height="15">&nbsp;</td><!--fila de espacio-->
                </tr>
                <tr>
                    <td width="200"><b>Ingrese Apellidos</b></td><!--Celda de la etiqueta Sección-->
                    <td><input type="text" name="apellidos" id="apellidos"></td>
                </tr>
                <tr>
                    <td height="15">&nbsp;</td><!--fila de espacio-->
                </tr>
                <tr>
                    <td width="200"><b>Ingrese Telefono</b></td><!--Celda de la etiqueta Sección-->
                    <td><input type="text" name="telefono" id="telefono"></td>
                </tr>
                <tr>
                    <td height="15">&nbsp;</td><!--fila de espacio-->
                </tr>
                <tr>
                    <td width="200"><b>Ingrese Correo</b></td><!--Celda de la etiqueta Sección-->
                    <td><input type="text" name="correo" id="correo"></td>
                </tr>  
                <tr>
                    <td height="45">&nbsp;</td><!--fila de espacio-->
                </tr>
                <tr>
                    <td>
                        <button type="submit" name="accion" value="ingresar" class="btn btn-success" onclick="alert('Vecino registrado!');">Registrar Vecino</button>
                    </td>
                </tr>
            </table>
        </form> 
    </body>
</html>
