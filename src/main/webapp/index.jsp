<%-- 
    Document   : index
    Created on : 05-06-2021, 20:32:52
    Author     : acamposm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <style>
            *{text-decoration:none; list-style:none; margin:0px; padding:0px; outline:none;}
            body{margin:0px; padding:0px; font-family: 'Open Sans', sans-serif;}
            section{width:100%; max-width:1200px; margin:0px auto; display:table; position:relative;}
            h1{margin:0px auto; display:table; font-size:26px; padding:40px 0px; color:#002e5b; text-align:center;}
            h1 span{font-weight:500;}

            header{width:100%; display:table; background-color:#AEBFFA; margin-bottom:50px;}
            #logo{float:left; font-size:24px; text-transform:uppercase; color:#002e5b; font-weight:600; padding:20px 0px;}
            nav{width:auto; float:left;}
            nav ul{display:table; float:left;}
            nav ul li{float:left;}
            nav ul li:last-child{padding-right:0px;}
            nav ul li a{color:#002e5b; font-size:18px; padding: 25px 20px; display:inline-block; transition: all 0.5s ease 0s;}
            nav ul li a:hover{background-color:#002e5b; color:#fde428; transition: all 0.5s ease 0s;}
            nav ul li a:hover i{color:#fde428; transition: all 0.5s ease 0s;}
            nav ul li a i{padding-right:10px; color:#002e5b; transition: all 0.5s ease 0s;}

            .toggle-menu ul{display:table; width:25px;}
            .toggle-menu ul li{width:100%; height:3px; background-color:#002e5b; margin-bottom:4px;}
            .toggle-menu ul li:last-child{margin-bottom:0px;}

            input[type=checkbox], label{display:none;}


        </style>
    </head>
    <body>
        <h1><span>Edificio GeoCentro Agustinas</span> </h1>

        <header>
            <section>
                <a href="yur" id="logo" target="_blank"></a>

                <label for="toggle-1" class="toggle-menu"><ul><li></li> <li></li> <li></li></ul></label>
                <input type="checkbox" id="toggle-1">

                <nav>
                    <ul>
                        <li><a href="LoginController"><i class="icon-home"></i>Ingresar</a></li>   
                        <li><a href="RegistroController"><i class="icon-home"></i>Registrar</a></li>
                    </ul>
                </nav>
        </header>

    </section>



</body>
</html>
