/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.dao.exceptions.NonexistentEntityException;
import com.dao.exceptions.PreexistingEntityException;
import com.entity.Informacionvecinos;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author acamposm
 */
public class InformacionvecinosJpaController implements Serializable {

    public InformacionvecinosJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Informacionvecinos informacionvecinos) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(informacionvecinos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findInformacionvecinos(informacionvecinos.getRut()) != null) {
                throw new PreexistingEntityException("Informacionvecinos " + informacionvecinos + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Informacionvecinos informacionvecinos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            informacionvecinos = em.merge(informacionvecinos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = informacionvecinos.getRut();
                if (findInformacionvecinos(id) == null) {
                    throw new NonexistentEntityException("The informacionvecinos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Informacionvecinos informacionvecinos;
            try {
                informacionvecinos = em.getReference(Informacionvecinos.class, id);
                informacionvecinos.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The informacionvecinos with id " + id + " no longer exists.", enfe);
            }
            em.remove(informacionvecinos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Informacionvecinos> findInformacionvecinosEntities() {
        return findInformacionvecinosEntities(true, -1, -1);
    }

    public List<Informacionvecinos> findInformacionvecinosEntities(int maxResults, int firstResult) {
        return findInformacionvecinosEntities(false, maxResults, firstResult);
    }

    private List<Informacionvecinos> findInformacionvecinosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Informacionvecinos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Informacionvecinos findInformacionvecinos(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Informacionvecinos.class, id);
        } finally {
            em.close();
        }
    }

    public int getInformacionvecinosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Informacionvecinos> rt = cq.from(Informacionvecinos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
