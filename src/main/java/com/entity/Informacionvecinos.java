/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author acamposm
 */
@Entity
@Table(name = "informacionvecinos")
@NamedQueries({
    @NamedQuery(name = "Informacionvecinos.findAll", query = "SELECT i FROM Informacionvecinos i"),
    @NamedQuery(name = "Informacionvecinos.findByRut", query = "SELECT i FROM Informacionvecinos i WHERE i.rut = :rut"),
    @NamedQuery(name = "Informacionvecinos.findByNombres", query = "SELECT i FROM Informacionvecinos i WHERE i.nombres = :nombres"),
    @NamedQuery(name = "Informacionvecinos.findByApellidos", query = "SELECT i FROM Informacionvecinos i WHERE i.apellidos = :apellidos"),
    @NamedQuery(name = "Informacionvecinos.findByTelefono", query = "SELECT i FROM Informacionvecinos i WHERE i.telefono = :telefono"),
    @NamedQuery(name = "Informacionvecinos.findByCorreo", query = "SELECT i FROM Informacionvecinos i WHERE i.correo = :correo")})
public class Informacionvecinos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "nombres")
    private String nombres;
    @Size(max = 2147483647)
    @Column(name = "apellidos")
    private String apellidos;
    @Size(max = 2147483647)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 2147483647)
    @Column(name = "correo")
    private String correo;

    public Informacionvecinos() {
    }

    public Informacionvecinos(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Informacionvecinos)) {
            return false;
        }
        Informacionvecinos other = (Informacionvecinos) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entity.Informacionvecinos[ rut=" + rut + " ]";
    }
    
}
